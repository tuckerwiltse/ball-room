// Tucker Wiltse
// Ball Room
// Get the square through the maze without touching the wall

#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

// CONSTANTS
const int xpin = 2;
const int ypin = 4;
const int ADC_num_bits = 12;  //12 bit ADC
const int ADC_int = 4095;    //2^12 bits
const int GND = 0;           //ref
const float SensorMaxV = 3;  //supply
const float sensorG = 3.0;       //sensor range -3 to 3 g

const int SCREEN_WIDTH = 128;
const int SCREEN_HEIGHT = 64;

const int NMOVE = 0;
const int MOVER = 1;
const int MOVEL = 2;
const int MOVEU = 3;
const int MOVED = 4;

//Vars to store data
float gX, gY;
int ballx, bally;
int boxAction;
int xpix = (SCREEN_WIDTH) / 2 - 10; //starts left of center of game room
int ypix = (SCREEN_HEIGHT-10) / 2 -2;
int gameOver = false;
int newBall = true;
int score = 0;

float refx, refy;
float rmv, lmv, umv, dmv;

//initialize display
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);

void setup() {
  Serial.begin(9600);
 
  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) {
    Serial.println(F("SSD1306 allocation failed"));
    for(;;);
  }
  delay(2000); // Pause for 2 seconds
  
  // Clear the buffer.
  display.clearDisplay();
  
  // Splash Screen:
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0, 10);
  display.println("Tucker Presents:"); //displays my name and game title
  display.setTextSize(2);
  display.setCursor(0, 30);
  display.println("BALL ROOM");
  display.display();
  
  delay(3000);//disp for 3 seconds

  //Calibration Screen
  display.clearDisplay();
  display.setTextSize(1);
  display.setCursor(0, 10);
  display.println("Calibrating . . .");   //Tells user not to move
  display.setCursor(0, 30);
  display.println("Please keep still");
  display.display();

  refx = mapf(analogRead(xpin), 0, ADC_int, -sensorG, sensorG); //calculates value of X without moving
  refy = -0.15; //magic number, don't know why. Was trying this -> mapf(analogRead(ypin), 0, ADC_int, sensorG, sensorG);
  rmv = refx + 0.05;
  lmv = refx - 0.05; //sets sensitivity for accelerometer input
  umv = refy + 0.05; 
  dmv = refy - 0.05;
  
  delay(2000);//disp for 2 seconds
}

void loop() {
  //get readings from accelerometer and convert to G value
  gX = mapf(analogRead(xpin), 0, ADC_int, -3.0, 3.0);
  gY = mapf(analogRead(ypin), 0, ADC_int, -sensorG, sensorG);

  //Game action, moves depending on accelerometer code
  if (gX > rmv){
    boxAction = MOVEU;
  } else if (gX < lmv){
    boxAction = MOVED;
  } else if (gY > umv){
    boxAction = MOVER;
  } else if (gY < dmv){
    boxAction = MOVEL;
  }

  Serial.println(refy);
  Serial.println(gY);
  
  display.clearDisplay(); //clear display

  //Display Score
  display.setTextSize(1);
  display.setCursor(0, 0);
  display.print("BALL ROOM | Score: "); display.print(score);

  //Game room setup
  display.drawRect(0, 10, SCREEN_WIDTH, SCREEN_HEIGHT-10, WHITE); //displays box around screen
  display.fillRect(ballx, bally, 2, 2, WHITE); //displays box
  display.drawLine(SCREEN_WIDTH/2, 30, SCREEN_WIDTH/2, 50, WHITE);

  //Box movement section
  moveBox(boxAction); //decides which direction box is moving
  display.fillRect(xpix, ypix, 5, 5, WHITE); //displays box
  display.drawCircle(ballx, bally, 2, WHITE); //displays box
  checkForActions();
  
  display.display();
  
  if (newBall){
    ballx = random(10, SCREEN_WIDTH-5);  //generates ball at a random place in the game room
    bally = random(20, SCREEN_HEIGHT-5);
    display.drawCircle(ballx, bally, 2, WHITE); //displays box    
    newBall = false;
  } else if (gameOver){
    //GAMEOVER Section
      display.clearDisplay();
      display.setCursor(8, 25); //center text and display it
      display.setTextSize(2);
      display.println("GAME OVER"); 
      display.display();  
      xpix = SCREEN_WIDTH / 2 - 2;
      ypix = (SCREEN_HEIGHT-10) / 2 - 2; //reset game values;
      gameOver = false;
      newBall = true;
      score = 0;
      delay(1000); //display for one second before restarting game
  }
  
  delay(50); //delay before next reading
}

//Extra Functions
float mapf(float x, float in_min, float in_max, float out_min, float out_max){
  return ((x - in_min) * (out_max - out_min) / (in_max - in_min)) + out_min;    //function that maps raw data to G value
}

void moveBox(int move){
  if (move == MOVER){
    xpix = xpix + 1;
  } else if (move == MOVEL){
    xpix = xpix - 1;
  } else if (move == MOVEU){
    ypix = ypix + 1;
  } else if (move == MOVED){
    ypix = ypix - 1;
  }
  return;
}

void checkForActions(){
  if (xpix > SCREEN_WIDTH-5 || xpix < 0 || ypix > SCREEN_HEIGHT-5 || ypix < 10) {//if hits border
    gameOver = true;
  } else if (xpix+4 >= SCREEN_WIDTH/2 && xpix <= SCREEN_WIDTH/2){ //if it hits center line
      if(ypix >= 30 && ypix <= 50){
        gameOver = true;
      }
  } else if(xpix+4 >= ballx && xpix <= ballx){ //if ball is within bounds of the box
      if(ypix+4 >= bally && ypix <= bally){
        score = score+1;
        newBall = true; //make a new ball and increment score
      }
  } 
}
